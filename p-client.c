#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <fcntl.h>
#define N 120

//процесс-клиент
int sockfd;
int SigHadnlr(int c) {
	int p = EOF;
	send(sockfd, &p, sizeof(char)*N, 0);
	close(sockfd);
	waitpid(-1, NULL, WNOHANG);
	exit(0);
}

int main(int argc, char **argv) {
	int len, pid;
	struct sockaddr_in myaddr;
	char *buf;


	signal(SIGINT, (void *)SigHadnlr);
	signal(SIGTERM, (void *)SigHadnlr);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("Client: socket\n");
		exit(1);
	}

	buf = malloc(sizeof(char)*N);

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(8010);				
	//myaddr.sin_addr.s_addr = inet_addr(argv[1]);	
	myaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	len = sizeof(myaddr);
	if (connect(sockfd, (struct sockaddr *)&myaddr, len ) == -1){
		perror("Client: connect\n");
		exit(1);
	}
	//считываем и отправляем имя клиента
	printf("Enter your name here: \n");
	fgets(buf, N, stdin);

	if (send(sockfd, buf, sizeof(char)*N, 0)<0) {
		perror("Client: send\n");
	}
	
	pid = fork();
	if (pid == 0) {	//процесс sender
		while (fgets(buf, N, stdin)) { 
			if (send(sockfd, buf, sizeof(char)*N, 0)<0) {
				perror("Client: send\n");
			}
		}
		kill(getppid(), SIGINT);
		close(sockfd);
		exit(0);
	}
	else {				//процесс reciever
		while (recv(sockfd, buf, sizeof(char)*N, 0)) {
			printf("%s", buf);
		}
		kill(pid, SIGINT);
		waitpid(-1, NULL, WNOHANG);
	}

	close(sockfd);
	exit(0);
}
