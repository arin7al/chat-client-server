Чат сервер

Процесс сборки проекта: 
запускаем makefile

all: server client

client: p-client.c
	gcc -g -Wall p-client.c -o client

server: p-server.c
	gcc -g -Wall p-server.c -o server

Процесс запуска проекта: 
после сборки через make
для запуска сервера, пишем ./server
для запуска клиента, пишем ./client 127.0.0.1

В файле с именем p-server.c находится код на языке си для работы сервера
В файле с именем p-client.c находится код на языке си для работы клиента