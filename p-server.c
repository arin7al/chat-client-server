#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/wait.h>	//wait4
#include <unistd.h>		//close
#include <signal.h>
#include <arpa/inet.h>
#define N 120
#define endword "/endchat\n"

//процесс-сервер
int ls, fdcount=0, *fdmas=NULL;
int SigHadnlr(int c) {
	close(ls);
	int j;
	for (j=0; j < fdcount; j++) {
		close(fdmas[j]);
	}
	//free(fdmas);
	exit(0);
}
int istoolong(char* str) {
	int i;

	for (i=0; i<N-1; i++) {
		if ((str[i]=='\n')||(str[i]=='\0')) {
			if (str[i]=='\n') {
				//printf("slesh en\n");
			}
			else
				//printf("slesh noll\n");
			return 0;			
		}
	}
	return 1;
}

int isnotempty(char* newname) {
	int i=0;
	while((newname[i]!= '\0')&&(newname[i]!= '\n')&&(i<N)) {
		if ((newname[i]!=' ')&&(newname[i]!='\t')) {
			return 1;
		}
		i++;
	}
	return 0;
}

int iscorrectname(char* newname, char **namemas, int fdcount) {
	int i;
	if (!istoolong(newname)) {
		if (fdcount>0) {
			for (i = 0; i < fdcount; i++) {
				if (strcmp(newname, namemas[i])==0) {
					return 0;
				}
			}	
		}
		return 1;	
	}
	else return 0;	
	
}
int main() {
	int isbind, sock1, addr1len, i, j, max_d, opt=1, stillong;
	struct sockaddr_in myaddr, addr1;
	char *buf, *buf1, **namemas=NULL, *namebuy=malloc(N);
	fd_set readfds, writefds;

	signal(SIGINT, (void *)SigHadnlr);
	signal(SIGTERM, (void *)SigHadnlr);
	addr1len = sizeof(addr1);
	
	buf = malloc(sizeof(char)*N);
	buf1 = malloc(sizeof(char)*N);

	ls = socket(AF_INET, SOCK_STREAM, 0);	
	if (ls == -1) {
		perror("Server: socket\n");
		exit(1);
	}

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(8010);						
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	setsockopt(ls, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));	//освобождаем номер порта
	
	isbind = bind(ls, (struct sockaddr *)&myaddr, sizeof(struct sockaddr_in));
	if (isbind == -1) {
		perror("Server: bind\n");
		exit(1);
	}

	if (listen(ls, 5) < 0) {
		perror("Server: listen\n");
		exit(1);
	}

	printf("Server: ready to accept new clients\n");
	while(1<2) {
		max_d = ls;

		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_SET(ls, &readfds);

		i = 0;
		if (fdcount>0) {
			for (i=0; i<fdcount; i++) {
			FD_SET(fdmas[i], &readfds);
			if (fdmas[i] > max_d)
				max_d = fdmas[i];
			}
		}
		
		int res = select(max_d + 1, &readfds, NULL, NULL, NULL); 
		if (res == 0) {
			//time-out 
			continue;
		}
		if (FD_ISSET(ls, &readfds)) {

			sock1 = accept(ls, (struct sockaddr *)&addr1, (socklen_t *)&addr1len);	
							//возвращает дескриптор нового сокета 
									//связанного с клиентом
			if (sock1 < 0) {
				perror("Server: accept\n");
				exit(1);
			}
try:			
			//получаем имя нового клиента
			if (recv(sock1, buf, sizeof(char)*N, 0)<0) {
				perror("Server: recv\n");
			}
			buf[strlen(buf)-1] = '\0';	
			if (!isnotempty(buf)) {
				//пустое имя
				strcpy(buf1, "The name is empty. Try again. \n");
				if (send(sock1, buf1, sizeof(char)*N, 0)<0) {
					perror("Server: send\n");
				}
				goto try;
			}
			else {
				if (iscorrectname(buf, namemas, fdcount)) {
					//сохраняем дескр нового сокета в массив
					fdcount++;
					fdmas = realloc(fdmas, sizeof(int)*fdcount);
					if (fdmas == NULL) {
						perror("Server: realloc\n");
					}
					fdmas[fdcount-1] = sock1;

					//сохраняем имя нового клиента в массив
					namemas = realloc(namemas, sizeof(char*)*fdcount);
					if (namemas == NULL) {
						perror("Server: realloc\n");
					}
					namemas[fdcount-1] = malloc(sizeof(char)*N);
					if (namemas[fdcount-1] == NULL) {
						perror("Server: malloc\n");
					}

					//buf[strlen(buf)-1] = '\0';			
					strcpy(namemas[fdcount-1], buf);

					strcpy(buf1, "Welcome to the chat! :)\n");
					if (send(sock1, buf1, sizeof(char)*N, 0)<0) {
						perror("Server: send\n");
					}

					//рассылаем имя вошедшего всем участникам			
					printf("%s entered the chat!\n", buf);

					for (i=0; i < fdcount - 1; i++) {				
						if (send(fdmas[i], buf, sizeof(char)*N, 0)<0) {
							perror("Server: send\n");
						}
						strcpy(buf1, " entered the chat!\n");
						if (send(fdmas[i], buf1, sizeof(char)*N, 0)<0) {
							perror("Server: send\n");
						}
					}
				}
				else {
					//такое имя уже есть
					strcpy(buf1, "The name is taken. Try again. \n");
					if (send(sock1, buf1, sizeof(char)*N, 0)<0) {
						perror("Server: send\n");
					}
					goto try;
				}
			}
		}

		for (i = 0; i < fdcount; i++) {
			
			if (FD_ISSET(fdmas[i], &readfds)) {
				//получаем сообщение				
				if (recv(fdmas[i], buf, sizeof(char)*N, 0)<0) {
					perror("Server: recv\n");
				}


				if (strcmp(buf, endword)==0) {
					strcpy(buf1, "Come back soon! :)\n");
					if (send(fdmas[i], buf1, sizeof(char)*N, 0)<0) {
						perror("Server: send\n");
					}
					//пользователь хочет закончить работу					
					close(fdmas[i]);
					strcpy(namebuy, namemas[i]);	
								
					for (j = i; j< fdcount - 1; j++) {
						fdmas[j] = fdmas[j+1];
					}
					for (j = i; j< fdcount - 1; j++) {
						namemas[j] = namemas[j+1];
					}
					fdcount--;
	
					//рассылаем имя покинувшего чат
					printf("%s left the chat!\n", namebuy);
									
					for (j=0; j < fdcount; j++) {
							strcpy(buf1, namebuy);	
							if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0) {
								perror("Server: send\n");
							}
							strcpy(buf1, " left the chat!\n");
							if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0) {
								perror("Server: send\n");
							}										
					}

				}

				else {
					if (buf[0]==EOF) {
						//printf("VSE OCHEN PLOHO\n");
						close(fdmas[i]);
						strcpy(namebuy, namemas[i]);	
									
						for (j = i; j< fdcount - 1; j++) {
							fdmas[j] = fdmas[j+1];
						}
						for (j = i; j< fdcount - 1; j++) {
							namemas[j] = namemas[j+1];
						}
						fdcount--;
							//рассылаем имя покинувшего чат
						printf("%s left the chat!\n", namebuy);
										
						for (j=0; j < fdcount; j++) {
								strcpy(buf1, namebuy);	
								if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0) {
									perror("Server: send\n");
								}
								strcpy(buf1, " left the chat!\n");
								if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0) {
									perror("Server: send\n");
								}										
						}

					}
					else {
						if (istoolong(buf)) {
							strcpy(buf1, "Your message is too long! Try again. \n");
							if (send(fdmas[i], buf1, sizeof(char)*N, 0)<0){
								perror("Server: send\n");
							}
							stillong = 1;
							while(stillong) {
								if (recv(fdmas[i], buf, sizeof(char)*N, 0)<0) {
									perror("Server: recv\n");
								}
								if(!istoolong(buf)) {
									stillong = 0;
								}
							}
						}
						else {
							printf("i got a message\n");
							//рассылаем сообщение
							for (j=0; j < fdcount; j++) {
								if (fdmas[j] != fdmas[i]) {
									strcpy(buf1, namemas[i]);	
									if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0){
										perror("Server: send\n");
									}
									strcpy(buf1, " wrote: ");
									if (send(fdmas[j], buf1, sizeof(char)*N, 0)<0) {
										perror("Server: send\n");
									}
									if (send(fdmas[j], buf, sizeof(char)*N, 0)<0) {
										perror("Server: send\n");
									}	
								}										
							}		
						}								
					}						
				}
			}
		}
	}
	close(ls);
	return 0;
}
